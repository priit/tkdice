import sys
from tkinter import *
from random import randint

game_count = 0
won = 0
cheater = 0

root = Tk()
root_width = 620
root_height = 260
pos_right = int(root.winfo_screenwidth() / 2 - root_width / 2)
pos_down = int(root.winfo_screenheight() / 2 - root_height / 2)
root.geometry("{}x{}+{}+{}".format(root_width, root_height, pos_right, pos_down))

root.title("Dice Game")
dice_min = 1

if(len(sys.argv) > 1):
	if(str(sys.argv[1]) == "--cheat"):
		cheater = 1
		root.title("Dice Game for Cheaters")
		dice_min = 4

canvas = Canvas(root)

def dice(face, x0, y0, x1, y1):
	canvas.create_rectangle(x0, y0, x1, y1, fill="yellow", outline="red", width=4)
	if face == 1:
		canvas.create_oval(x0+40, y0+40, x1-40, y1-40, fill="red", width=0)
	elif face == 2:
		canvas.create_oval(x0+10, y0+10, x1-70, y1-70, fill="red", width=0)
		canvas.create_oval(x0+70, y0+70, x1-10, y1-10, fill="red", width=0)
	elif face == 3:
		canvas.create_oval(x0+10, y0+10, x1-70, y1-70, fill="red", width=0)
		canvas.create_oval(x0+40, y0+40, x1-40, y1-40, fill="red", width=0)
		canvas.create_oval(x0+70, y0+70, x1-10, y1-10, fill="red", width=0)
	elif face == 4:
		canvas.create_oval(x0+10, y0+10, x1-70, y1-70, fill="red", width=0)
		canvas.create_oval(x0+10, y0+70, x1-70, y1-10, fill="red", width=0)
		canvas.create_oval(x0+70, y0+10, x1-10, y1-70, fill="red", width=0)
		canvas.create_oval(x0+70, y0+70, x1-10, y1-10, fill="red", width=0)
	elif face == 5:
		canvas.create_oval(x0+10, y0+10, x1-70, y1-70, fill="red", width=0)
		canvas.create_oval(x0+10, y0+70, x1-70, y1-10, fill="red", width=0)
		canvas.create_oval(x0+40, y0+40, x1-40, y1-40, fill="red", width=0)
		canvas.create_oval(x0+70, y0+10, x1-10, y1-70, fill="red", width=0)
		canvas.create_oval(x0+70, y0+70, x1-10, y1-10, fill="red", width=0)
	elif face == 6:
		canvas.create_oval(x0+10, y0+10, x1-70, y1-70, fill="red", width=0)
		canvas.create_oval(x0+10, y0+40, x1-70, y1-40, fill="red", width=0)
		canvas.create_oval(x0+10, y0+70, x1-70, y1-10, fill="red", width=0)
		canvas.create_oval(x0+70, y0+10, x1-10, y1-70, fill="red", width=0)
		canvas.create_oval(x0+70, y0+40, x1-10, y1-40, fill="red", width=0)
		canvas.create_oval(x0+70, y0+70, x1-10, y1-10, fill="red", width=0)

def stop_program():
	root.destroy()

def roll_dice():

	global game_count
	global won

	canvas.delete('all')

	x0 = y0 = 20
	x1 = y1 = 120
	total = 0
	ai_total = 0
	ai_dice = []

	for i in range(1, 6):
		roll = randint(dice_min, 6)
		dice(roll, x0, y0, x1, y1)
		ai_roll = randint(1, 6)
		ai_dice.append(ai_roll)
		total += roll
		ai_total += ai_roll
		x0 += 120
		x1 += 120

	your_result = "You got " + str(total) + "."
	ai_result = "Computer rolled " + \
		str(ai_dice[0]) + ", " + \
		str(ai_dice[1]) + ", " + \
		str(ai_dice[2]) + ", " + \
		str(ai_dice[3]) + ", " + \
		str(ai_dice[4]) + " and got " + str(ai_total) + "."

	if total == ai_total:
		congrats = "It's a tie!"
		your_fill = "black"
		ai_fill = "black"
	elif total > ai_total:
		won += 1
		if cheater == 1:
			congrats = "You won, bloody cheater!"
		else:
			congrats = "You won!"
		your_fill = "green"
		ai_fill = "red"
	else:
		if cheater == 1:
			congrats = "Haha, you can't even win with cheating!"
		else:
			congrats = "You lose!"
		your_fill = "red"
		ai_fill = "green"

	game_count += 1
	canvas.create_text(20, 140, text=your_result, fill=your_fill, font=('Helvetica 14 bold'), anchor="w")
	canvas.create_text(20, 160, text=ai_result, fill=ai_fill, font=('Helvetica 14 bold'), anchor="w")
	canvas.create_text(20, 190, text=congrats, fill='black', font=('Helvetica 20 bold'), anchor="w")
	canvas.create_text(190, 226, text="You've won " + str(won) + " out of " + str(game_count) + " games.", fill='black', font=('Helvetica 18'), anchor="w")

button = Button(root, text='Roll Again!', command=roll_dice)
button.place(x=20, y=210)
button_exit = Button(root, text='Quit', command=stop_program)
button_exit.place(x=120, y=210)

canvas.pack(fill=BOTH, expand=1)

roll_dice()
root.mainloop()
